require "common/Vec"
local camera = {}

function camera:update(pos)
	self.x = pos.x - love.graphics.getWidth()/2
	self.y = pos.y - love.graphics.getHeight()/2
end

function camera:set()
	love.graphics.push()
	love.graphics.rotate(-self.rotation)
	love.graphics.scale(self.scale[1], self.scale[2])
	love.graphics.translate(-self.x, -self.y)
end

function camera.unset()
	love.graphics.pop()
end

function camera:new()
	self.x = -1000
	self.y = -1000
	self.rotation = 0
	self.scale = {1,1}
end

return camera