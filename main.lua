local scene = require "scene"

function love.load(path)
	math.randomseed(os.time())
	local chunk = love.filesystem.load("scene/"..path[1]..".lua")
	local scene_data = chunk()
	scene:load(scene_data)
end

function love.update(dt)
	scene.update(dt)
end

function love.draw()
	scene:draw()
end