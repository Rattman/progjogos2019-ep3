require "common/vec"

local entities = {}

entities.position 	= {}
entities.movement 	= {}
entities.body 		= {}
entities.control 	= {}
entities.field 		= {}
entities.charge 	= {}

entities.size = 0

function entities:add_entity(info)
	self.size = self.size + 1
	local id = self.size

	if info.position then
		if info.position.point then
			self.position[id] = info.position.point
		else
			self.position[id] = "ns" --not_specified
		end
	end

	if info.movement then
		if info.movement.motion then
			self.movement[id] = info.movement.motion
		else
			self.movement[id] = "ns" --not_specified
		end
	end

	if info.body then
		if info.body.size then
			self.body[id] = info.body.size
		else
			self.body[id] = 8
		end
	end

	if info.control then
		self.control[id] = {}
		if info.control.acceleration then
			self.control[id].acceleration = info.control.acceleration
		else
			self.control[id].acceleration = 0.0
		end

		if info.control.max_speed then
			self.control[id].max_speed = info.control.max_speed
		else
			self.control[id].max_speed = 50.0
		end

		self.control[id].direction = 0.0
	end

	if info.field then
		if info.field.strength then
			self.field[id] = info.field.strength
		else
			self.field[id] = 1
		end
	end

	if info.charge then
		if info.charge.strength then
			self.charge[id] = info.charge.strength
		else
			self.charge[id] = 1
		end
	end

end

return entities