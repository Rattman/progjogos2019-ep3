local Vec = require "common/Vec"

local entities = require "entities"
local physics = require "physics"
local camera = require "camera"
local control = require "control"

local scene = {}

function scene:load(data)
	for _,info in pairs(data) do
		local chunk = love.filesystem.load("entity/"..info.entity..".lua")
		local entity = chunk()
		for _= 1, info.n, 1 do
			entities:add_entity(entity)
		end
	end

	self.set_positions()
	self.set_movement()
	camera:new()

	self.control_triangle = love.graphics.newMesh{{15, 0}, {-10,-10}, {-10,10}}
end

function scene.set_positions()
	for i,p in pairs(entities.position) do
		if p == "ns" then
			local size = entities.body[i] or 8
			local total_radius = 1000-(size+1)
			local x = math.floor(math.random(-total_radius,total_radius))
			local y = math.floor(math.random(-math.sqrt(total_radius^2-x^2),math.sqrt(total_radius^2-x^2)))
			entities.position[i] = Vec(x,y)
		end
	end
end

function scene.set_movement()
	for i,m in pairs(entities.movement) do
		if m == "ns" then
			local max_m = 100
			entities.movement[i] = Vec(math.random(-max_m,max_m),math.random(-max_m,max_m))
		end
	end
end

function scene.update(dt)
	control.update_move(entities, dt)

	physics.update_pos(entities, dt)
	physics.check_collitions(entities, dt)
	physics.apply_charge(entities, dt)

	for i,_ in pairs(entities.control) do
		camera:update(entities.position[i])
	end
end

function scene:draw()
	if #entities.control > 0 then
		camera:set()
	else
		local w_center = love.graphics.getWidth()/2
		local h_center = love.graphics.getHeight()/2
		love.graphics.translate(w_center, h_center)
	end

	love.graphics.setColor(1,1,1)
	love.graphics.circle("line", 0, 0, 1000)
	for i,p in pairs(entities.position) do

		local size = entities.body[i]
		local field = entities.field[i]
		local charge = entities.charge[i]

		love.graphics.setColor(0,1,0)

		if field ~= nil then
			if field > 0 then
				love.graphics.setColor(0,0,1)
			elseif field < 0 then
				love.graphics.setColor(1,0,0)
			end
		end

		if size then
			love.graphics.circle("fill", p.x, p.y, size)
		end

		if field then
			if size then love.graphics.setColor(1,1,1) end
			love.graphics.circle("line", p.x, p.y, field)
		end

		if size == nil and field == nil then
			love.graphics.circle("line", p.x, p.y, 8)
		end

		if charge then
			love.graphics.setColor(0,1,0)
			if charge > 0 then
				love.graphics.setColor(0,0,1)
			elseif charge < 0 then
				love.graphics.setColor(1,0,0)
			end

			love.graphics.circle("line", p.x-8, p.y, 4)
			love.graphics.circle("line", p.x+8, p.y, 4)
		end

		if entities.control[i] ~= nil then
			love.graphics.setColor(1,1,1)
			love.graphics.draw(self.control_triangle, p.x, p.y, entities.control[i].direction:getAngle())
		end

	end
	if #entities.control > 0 then
		camera:unset()
	end
end

return scene