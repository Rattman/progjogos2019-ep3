require "common/Vec"

local physics = {}

function physics.update_pos(entities, dt)
	local position = entities.position
	local body = entities.body
	for i,m in pairs(entities.movement) do
		local size = body[i] or 8

		position[i] = position[i] + m*dt

		if position[i]:length() >= 1000 - size then
			position[i] = -position[i] + m*dt
			position[i] = position[i] - position[i]:normalized()*(size)
		end
	end
end

function physics.check_collitions(entities)
	local position = entities.position
	local movement = entities.movement
	local body = entities.body

	for i,sizei in pairs(body) do
		for j,sizej in pairs(body) do
			if(j > i) then

				local dist = (position[i]-position[j]):length() - (sizei + sizej)

				if dist <= 0 then

					local d = (position[i]-position[j]):normalized()

					position[i] = position[i] + d*(-dist)

					if movement[i] ~= nil then
						movement[i] = movement[i] - d*(d:dot(movement[i]))
					elseif(movement[j] ~= nil) then
						movement[j] = movement[j] - d*(d:dot(movement[j]))
					end
				end

			end
		end
	end
end


function physics.apply_charge(entities, dt)
	local movement = entities.movement
	local charge = entities.charge
	local field = entities.field

	local C = 1000

	for i,_ in pairs(field) do
		for j,_ in pairs(charge) do
			if(movement[j]~=nil) then

				local m = entities.body[j] or 1
				local dist = entities.position[j]-entities.position[i]

				local F = (dist/(dist:length()^2))*C*field[i]*charge[j]
				local a = F/m

				if a.x > 0 then
					movement[j] = movement[j] + a*dt
				end

			end
		end
	end

end

return physics