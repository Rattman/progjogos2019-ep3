local Vec = require "common/Vec"

local control = {}

function control.update_move(entities, dt)

	local direction = Vec(0,0)

	if love.keyboard.isDown("down") then
		direction = direction + Vec(0,1)
	end

	if love.keyboard.isDown("up") then
		direction = direction + Vec(0,-1)
	end

	if love.keyboard.isDown("left") then
		direction = direction + Vec(-1,0)
	end

	if love.keyboard.isDown("right") then
		direction = direction + Vec(1,0)
	end

	for i,c in pairs(entities.control) do
		entities.movement[i] = entities.movement[i] + direction*c.acceleration*dt
		entities.movement[i]:clamp(c.max_speed)
		c.direction = direction
	end

end

return control